# Repository Containing Programming Projects:
  \+ cwk1 (contains quadtree project)  
    \+ src (c source code)  
      \+ obj (object files)  
    \+ inc (header files)  
    
  \+ cwk2 (contains second project, paint program)  
  	\+ src (c source code)  
  	  \+ obj (object files)  
  	  \+ assets (assets used for UI in paint program)  
  	\+ inc (header files)  
  	\+ saves (paint program saves images to here)  
  	\+ test (some initial SDL testing occured here)  
