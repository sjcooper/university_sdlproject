#ifndef TREE_H
#define TREE_H

/**
 * Data Structure Definitions
 */
 
typedef struct treenode {
  // node data
  int level;
  double xy[2];
  struct treenode *child[4];
  struct treenode *nextLeaf;
} Node;

typedef struct leaflist {
  Node *topLeaf;
} LeafList;

typedef struct quadtree {
  Node *head;
  LeafList *list;
} Quadtree;

#endif
