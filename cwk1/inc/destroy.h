#ifndef DESTROY_H
#define DESTROY_H
#include "treeNode.h"

//tree destruction
void destroyNode(Node *node);
void destroyProcedure(Node *subtreeRoot);
void destroyTree(Quadtree *tree);

#endif
