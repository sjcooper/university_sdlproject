//header guards
#ifndef TREEOPS_H
#define TREEOPS_H
#include "treeNode.h"

//tree growth
Node *makeNode(double x, double y, int level);
void makeChildren(Node *parent);
//expands all leaf nodes under the given node into children
void growTree(Node *node);
//list version of growTree() function
void listGrowTree(Quadtree *tree);

//initialises tree
Quadtree *initTree();

//traverses linked list, adding children based on data passed to it
int passThrough(Node* leaf, double tolerance, int choice);

//generates data dependent tree structure based on existing models
void generateModel(Quadtree *tree, double tolerance, int choice);
#endif
