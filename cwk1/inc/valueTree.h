#ifndef VALUE_H
#define VALUE_H

#include  "treeNode.h"
#include <stdbool.h>

/**
 * Function prototypes for generating a data dependent quadtree
 */

double dataFunction( double x, double y, int choice );
bool indicator( Node *node, double tolerance, int choice );

#endif
