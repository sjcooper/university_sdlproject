#ifndef PRINT_H
#define PRINT_H
#include "treeNode.h"
//tree printing
void printOut(FILE *fp, Node *node);
void writeTree(Node *head);
void writeNode(FILE *fp, Node *node);

//testing
void listWriteNode( FILE *fp, Node *leaf );
void listWriteTree( Quadtree *tree );


#endif
