#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../inc/print.h"

/**
 * Tree Output
 */

void printOut(FILE *fp, Node *node) {
  // node data
  double x = node->xy[0];
  double y = node->xy[1];
  int level = node->level;
  double h = pow(2.0,-level);

  // print out the corner points
  fprintf(fp, " %g %g\n",x,y);
  fprintf(fp, " %g %g\n",x+h,y);
  fprintf(fp, " %g %g\n",x+h,y+h);
  fprintf(fp, " %g %g\n",x,y+h);
  fprintf(fp, " %g %g\n\n",x,y);

  return;
}

// open a file and prepare to write
void writeTree( Node *head ) {
  if(head == NULL) {
    printf("Cannot write NULL tree\n");
    return;
  }
  // opens gnuplot graph plotting file
  FILE *fp = fopen("quad.out","w");
  writeNode(fp,head);
  fclose(fp);
  return;
}

// recursively search for leaf nodes, print leaf nodes it finds
void writeNode( FILE *fp, Node *node ) {
  int i;
  if( node->child[0] == NULL ) {
    printOut( fp, node );
  }
  else {
    for ( i=0; i<4; ++i ) {
      writeNode(fp, node->child[i]);
    }
  }
  return;
}

//prints linked list of leaf nodes, implemented seperately from writeNode
void listWriteNode( FILE *fp, Node *leaf ) {
  while(leaf != NULL) {
      printOut(fp, leaf);
      leaf = leaf->nextLeaf;
  }
  return;
}

//prints tree to file provided, calls listWriteNode
void listWriteTree( Quadtree *tree ) {
  if(tree->list == NULL) {
    printf("Cannot write NULL list, make sure to create list.\n");
    return;
  }
  FILE *fp = fopen("quad.out","w");
  listWriteNode(fp, tree->list->topLeaf);
  fclose(fp);
  return;
}
