#include <SDL2/SDL.h>
#include <stdio.h>

#include "SDLopen.h"

int main(int argc, char *argv[]) {

  // loop condition variable for quitting
  bool quit = false;
  //event handler
  SDL_Event event;

  // start SDL and try initialise window
  if(!init()) {
    printf("Failed to initialise!\n");
  } else {
    // try load image
    if(!loadImage(argv[1])) {
      printf("Failed to load image!\n");
    } else {
      // main event loop
      while(!quit) {
        // event handle queue
        while (SDL_PollEvent(&event) != 0) {
          // if user presses the 'cross'
          if(event.type == SDL_QUIT) {
            quit = true;
          }
        }
        displayImage();
      }
    }
  }
  // close SDL
  closeSDL();
  return 0;
}
