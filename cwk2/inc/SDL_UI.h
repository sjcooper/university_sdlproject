/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#ifndef SDLUI_H
#define SDLUI_H

// displays a chosen message on-screen next to buttons
void displayMessage(char* message);
// loads provided spritesheet as a texture
void initPalette();
// renders spritesheet accordingly, spread at bottom of window
void displayPalette();


#endif
