/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#ifndef SDLRENDER_H
#define SDLRENDER_H

// sets brush colour to black
void initBrushColour();
// returns the number of the button the cursor is over (is passed mouse coordinates)
int isOverButton(int x, int y);
// makes changes depending on the button pressed (should be passed the return value of isOverButton())
void buttonChange(int c);
// draws line between old mouse position and new mouse position
void draw(int* w, int* x);
// presents current renderer state
void render();

#endif
