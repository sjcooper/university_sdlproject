/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string.h>
#include <time.h>

#include "../inc/SDL_Structures.h"
#include "../inc/SDL_IO.h"

// variable to hold filename
static char fileName[32];

/* returns current time + .bmp as a string */
void timeFileName() {
  // get current time
  time_t currentTime;
  struct tm * localTime;
  time(&currentTime);
  localTime = localtime(&currentTime);
  // put time in format below into fileName string
  strftime(fileName, sizeof(fileName), "../saves/%T %d.%m.%y", localTime);
  return;
}

/* saves current worksheet to bmp file */
void save() {
  // change filename to current time
  timeFileName();
  // create surface based on screen information
  SDL_Surface *capture = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
  SDL_LockSurface(capture);
  // render pixel information to surface
  SDL_RenderReadPixels(renderer, NULL, SDL_PIXELFORMAT_ARGB8888, capture->pixels, capture->pitch);
  // save surface as bmp file (based on current time)
  SDL_SaveBMP(capture, fileName);
  SDL_UnlockSurface(capture);
  // get rid of surface after use
  SDL_FreeSurface(capture);
  return;
}
