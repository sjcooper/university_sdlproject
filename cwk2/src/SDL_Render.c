/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#define CENTRE_FILE

#include <SDL2/SDL.h>

#include "../inc/SDL_Structures.h"
#include "../inc/SDL_Render.h"
#include "../inc/SDL_UI.h"
#include "../inc/SDL_IO.h"

/* sets initial brush colour to black */
void initBrushColour() {
  // sets brush colour to black
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  return;
}


/* returns the number of the button the cursor is over */
int isOverButton(int x, int y) {
  int init = 20;
  for(int i = 1; i < NUM_BUTTONS + 1; i++) {
    if((x > init) && (x < init + 40) && (y > SCREEN_HEIGHT + 20) && (y < SCREEN_HEIGHT + 60)) {
      return i;
    } else {
      init += 50;
    }
  }
}


/* makes changes depending on the button pressed */
void buttonChange(int c) {
  switch (c) {
    case 1: displayMessage("Red");
            SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);     // RED
    break;
    case 2: displayMessage("Orange");
            SDL_SetRenderDrawColor(renderer, 255, 161, 0, 255);   // ORANGE
    break;
    case 3: displayMessage("Yellow");
            SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);   // YELLOW
    break;
    case 4: displayMessage("Green");
            SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);      // GREEN
    break;
    case 5: displayMessage("Cyan");
            SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255);    // CYAN
    break;
    case 6: displayMessage("Blue");
            SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);      // BLUE
    break;
    case 7: displayMessage("Purple");
            SDL_SetRenderDrawColor(renderer, 128, 0, 255, 255);    // PURPLE
    break;
    case 8: displayMessage("Magenta");
            SDL_SetRenderDrawColor(renderer, 255, 0, 255, 255);    // MAGENTA
    break;
    case 9: displayMessage("White");
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);  // WHITE
    break;
    case 10: displayMessage("Black");
             SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);       // BLACK
    break;
    case 11: brushSize = 1;                                        // SMALL BRUSH
    break;
    case 12: brushSize = 3;                                        // MEDIUM BRUSH
    break;
    case 13: brushSize = 5;                                        // LARGE BRUSH
    break;
    case 15: save();                                               // SAVE
             // fallthrough to prevent rendering errors
    case 14: SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // CLEAR SCREEN
             // fill canvas with white
             SDL_RenderClear(renderer);
             // render palette again
             displayPalette();
             displayMessage("Black");
             // set drawing colour to black
             SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    break;
  }
}


/* draws line between old mouse position and new mouse position */
void draw(int* w, int* x) {
  // create variables to hold current mouse position
  int y, z;
  // apply position
  SDL_GetMouseState(&y, &z);
  // render line depending on brush size
  for(int i = 0; i < brushSize; i++) {
    for(int j = 0; j < brushSize; j++) {
      SDL_RenderDrawPoint(renderer, *w+i, *x+j);
      SDL_RenderDrawLine(renderer, *w+i, *x+j, y+i, z+j);
    }
  }
  // update old mouse position to current one
  *w = y;
  *x = z;
  return;
}

/* presents current renderer state */
void render() {
  SDL_RenderPresent(renderer);
}
